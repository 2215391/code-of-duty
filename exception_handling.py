def main():
    print_list_items()


def print_list_items():
    try:
        list1 = ["apple", "banana", "mango"]
        print(list1[3])
    except IndexError:
        print("element out of bounds")


if __name__ == "__main__":
    main()
