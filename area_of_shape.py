def main():
    shape = get_shape()
    print(get_area_of_shape(shape))


def get_shape():
    print("Enter the shape to be calculated\n")
    print("Triangle\n")
    print("Square\n")
    print("Rectangle\n")
    shape = input("Shape: ")
    return shape


def get_area_of_shape(shape):
    if shape == "Square":
        s = int(input("Enter length of side: "))
        return get_area_of_square(s)
    elif shape == "Triangle":
        b = int(input("Enter the length of the triangle's base: "))
        h = int(input("Enter the length of the triangle's height: "))
        return get_area_of_triangle(b, h)
    elif shape == "Rectangle":
        l = int(input("Enter the length of the rectangle: "))
        w = int(input("Enter the width of the rectangle: "))
        return get_area_of_rectangle(l, w)
    else:
        print("Shape {} does not exist in the list".format(shape))


def get_area_of_square(s):
    return s ** 2


def get_area_of_rectangle(l, w):
    return l * w


def get_area_of_triangle(b, h):
    return (b * h) * 0.5


if __name__ == "__main__":
    main()
