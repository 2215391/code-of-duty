def main():
    introduction = "My name is {} and I am {} years old!"
    name, age = get_user_details()
    print(introduction.format(name, age))


def get_user_details():
    name = input("Enter your name: ")
    age = input("Enter your age: ")
    return name, age


if __name__ == "__main__":
    main()
