class Student:
    # constructor
    def __init__(self, name, id_number, course, year):
        self.name = name
        self.id_number = id_number
        self.course = course
        self.year = year

    # prints out the information of a student
    def show_info(self):
        print("Name: ", self.name)
        print("ID Number: ", self.id_number)
        print("Course: ", self.course)
        print("Year: ", self.year)

    def set_name(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def set_id_number(self, id_number):
        self.id_number = id_number

    def get_id_number(self):
        return self.id_number

    def set_course(self, course):
        self.course = course

    def get_course(self):
        return self.course

    def set_year(self, year):
        self.year = year

    def get_year(self):
        return self.year


student1 = Student("Jeffrey", 123, "BSCS", 2)
student1.show_info()
